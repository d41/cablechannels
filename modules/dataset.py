# -*- coding: utf-8 -*-

import csv

__author__ = "d41"


class __dialect(object):
    delimiter = ':'
    quotechar = '"'
    escapechar = None
    doublequote = True
    skipinitialspace = True
    lineterminator = '\r\n'
    quoting = csv.QUOTE_MINIMAL


def prepare_round(filename):
    information = {}
    try:
        with open(filename, "r", encoding="utf-8") as file:
            datalist = csv.reader(file.readlines(), dialect=__dialect)
            for row in datalist:
                name, external, internal = row
                if name not in information:
                    information[name] = []
                information[name].append(
                    (external, float(internal))
                )
    except FileNotFoundError as err:
        print(err)

    return information


def prepare_rectangular(filename):
    information = {}
    try:
        with open(filename, "r", encoding="utf-8") as file:
            datalist = csv.reader(file.readlines(), dialect=__dialect)
            for row in datalist:
                name, section, width, height = row
                information[name] = int(section), int(width), int(height)
    except FileNotFoundError as err:
        print(err)

    return information

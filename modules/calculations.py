# -*- coding: utf-8 -*-

import math

__author__ = "d41"
__doc__ = """\
Calculation functions removed from UI.

Please use same dimensional units everywhere, i.e. mm.
"""


def section_round(diameter):
    """
    Round section area.

    :param diameter: diameter of channel or round cable
    :return: area

    >>> round(section_round(10), 2)
    78.54
    >>> round(section_round(20), 2)
    314.16
    """
    return math.pi * pow(diameter / 2.0, 2)


def section_rectangular(width, height):
    """
    Rectangular shape section area.

    :param width: width of cable channel
    :param height: height of cable channel
    :return: area

    >>> section_rectangular(12, 6)
    72

    >>> section_rectangular(8, 0)
    0
    """
    return height * width


def count_cables(channel, cable, consumption):
    """
    Count cables that fit channel based on crossection of cable and channel, and required consumption percent.
    Simplified version, because not take in account gaps between cables.

    :param channel: channel section area
    :param cable: cable section area
    :param consumption: percentage of channel fill with cables
    :return: (count, absurd) amount of cables

    >>> count_cables(100, 10, 40)
    (4, 10)
    """
    count = math.floor((channel / cable) * (consumption / 100))
    absurd = int(channel / cable)
    if count == 0:
        if cable < channel:
            count = 1
    return count, absurd


def lineup_rectangular(count, diameter, height):
    """
    Get lineout of cables inside rectangular cable channel.

    :param count: cables amount
    :param diameter: cable diameter
    :param height: channel height
    :return: count by side X, count by side Y, additional items
    """
    side_y = math.floor(height / diameter)
    side_x = math.floor(count / side_y)
    tail = count - side_x * side_y
    return side_x, side_y, tail


def lineup_round(count, diameter, height):
    """
    Get lineout of cables inside round cable channel.

    :param count: cables amount
    :param diameter: - ignored, to make same signature with lineup_rectangular
    :param height: - ignored, to make same signature with lineup_rectangular
    :return: count by side X, count by side Y, additional items
    """
    line = math.floor(math.sqrt(count))
    tail = count - line * line
    return line, line, tail

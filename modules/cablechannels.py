# -*- coding: utf-8 -*-


from PyQt5 import QtCore, QtWidgets, QtGui

from modules import cablechannels_ui
from modules import calculations as calc
from modules import dataset

__author__ = "d41"


class CableChannels(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.ui = cablechannels_ui.Ui_Form()
        self.ui.setupUi(self)

        # setup pens for scheme
        self.pen_channel = QtGui.QPen(QtCore.Qt.red)
        self.pen_internal = QtGui.QPen(QtCore.Qt.green)
        self.pen_cable = QtGui.QPen(QtCore.Qt.black)

        # calculate scale factor
        self.k_mm = QtGui.QGuiApplication.primaryScreen().physicalDotsPerInch() / 25.4  # dotsPerMM

        self.info_round = dataset.prepare_round("data/round.csv")
        self.ui.round_type.addItem("Задать вручную")
        self.ui.round_type.addItems(self.info_round.keys())
        self.ui.external_diameter.setEnabled(False)

        self.info_rectangular = dataset.prepare_rectangular("data/rectangular.csv")
        self.ui.rectangular_type.addItem("Задать вручную")
        for name in self.info_rectangular:
            self.ui.rectangular_type.addItem(name, self.info_rectangular[name])

    @QtCore.pyqtSlot(str)
    def on_round_type_currentTextChanged(self, value):
        self.ui.external_diameter.clear()
        if value in self.info_round:
            for ext_diameter, in_diameter in self.info_round[value]:
                self.ui.external_diameter.addItem(ext_diameter, in_diameter)
            self.ui.external_diameter.setEnabled(True)
            self.ui.channel_diameter.setReadOnly(True)
        else:
            self.ui.external_diameter.setEnabled(False)
            self.ui.channel_diameter.setReadOnly(False)

    @QtCore.pyqtSlot(int)
    def on_external_diameter_currentIndexChanged(self, index):
        if index != -1:
            in_diameter = self.ui.external_diameter.itemData(index)
            self.ui.channel_diameter.setValue(in_diameter)

    @QtCore.pyqtSlot(int)
    def on_rectangular_type_currentIndexChanged(self, index):
        if index == 0:
            self.ui.channel_width.setEnabled(True)
            self.ui.channel_height.setEnabled(True)
            self.ui.channel_info.setEnabled(False)
            self.ui.channel_info.setValue(0)

        else:
            self.ui.channel_width.setEnabled(False)
            self.ui.channel_height.setEnabled(False)
            self.ui.channel_info.setEnabled(True)
            section, width, height = self.ui.rectangular_type.itemData(index)
            self.ui.channel_width.setValue(width)
            self.ui.channel_height.setValue(height)
            self.ui.channel_info.setValue(section)

    @QtCore.pyqtSlot()
    def on_calc_round_clicked(self):
        # get input data
        cable_diameter = self.ui.cable_diameter.value()
        channel_consumption = self.ui.channel_consumption.value()
        channel_diameter = self.ui.channel_diameter.value()
        if self.ui.external_diameter.currentIndex() == -1:
            ext_channel_diameter = self.ui.channel_diameter.value() + 2
        else:
            ext_channel_diameter = int(self.ui.external_diameter.currentText())

        # main calculations
        section_cable = calc.section_round(cable_diameter)
        section_channel = calc.section_round(channel_diameter)
        cables, cables_absurd = calc.count_cables(section_channel, section_cable, channel_consumption)

        # set output data
        self.ui.section_cable.setValue(section_cable)
        self.ui.section_channel.setValue(section_channel)
        self.ui.cables.setValue(cables)
        self.ui.cables_absurd.setValue(cables_absurd)

        self.draw_round_scheme(cables, cable_diameter, ext_channel_diameter, channel_diameter)

    @QtCore.pyqtSlot()
    def on_calc_rectangular_clicked(self):
        # get input data
        cable_diameter = self.ui.cable_diameter.value()
        channel_consumption = self.ui.channel_consumption.value()
        channel_width = self.ui.channel_width.value()
        channel_height = self.ui.channel_height.value()
        channel_info = self.ui.channel_info.value()

        # main calculations
        section_cable = calc.section_round(cable_diameter)
        if channel_info == 0:
            section_channel = calc.section_rectangular(channel_width, channel_height)
        else:
            section_channel = channel_info
        cables, cables_absurd = calc.count_cables(section_channel, section_cable, channel_consumption)

        # set output data
        self.ui.section_cable.setValue(section_cable)
        self.ui.section_channel.setValue(section_channel)
        self.ui.cables.setValue(cables)
        self.ui.cables_absurd.setValue(cables_absurd)

        self.draw_rectangular_scheme(cables, cable_diameter, channel_width, channel_height)

    def draw_round_scheme(self, cables, cable_diameter, ext_diameter, in_diameter):
        # drawing scheme
        scene = QtWidgets.QGraphicsScene(parent=self.ui.graphicsView)

        self.draw_round(scene, ext_diameter, in_diameter)
        self.draw_cables(scene, cables, cable_diameter, in_diameter, calc.lineup_round)

        self.ui.graphicsView.setScene(scene)

    def draw_rectangular_scheme(self, cables, cable_diameter, channel_width, channel_height):
        # drawing scheme
        scene = QtWidgets.QGraphicsScene(parent=self.ui.graphicsView)

        self.draw_rectangle(scene, channel_width, channel_height)
        self.draw_cables(scene, cables, cable_diameter, channel_height, calc.lineup_rectangular)

        self.ui.graphicsView.setScene(scene)

    def draw_round(self, scene, ext_diameter, in_diameter):
        ext_size = ext_diameter * self.k_mm
        scene.addEllipse(
            QtCore.QRectF(0, 0, ext_size, ext_size),
            self.pen_channel
        )
        shift = (ext_diameter - in_diameter) / 2 * self.k_mm
        in_size = ext_size - shift * 2
        scene.addEllipse(
            QtCore.QRectF(shift, shift, in_size, in_size),
            self.pen_internal
        )

    def draw_rectangle(self, scene, width, height):
        scene.addRect(
            QtCore.QRectF(0, 0, width * self.k_mm, height * self.k_mm),
            self.pen_channel
        )
        thickness = 2
        thickness = 1.7 if width <= 80 else thickness
        thickness = 1.3 if width <= 60 else thickness
        thickness = 1.1 if width <= 30 else thickness

        scene.addRect(
            QtCore.QRectF(
                self.k_mm * thickness,
                self.k_mm * thickness,
                self.k_mm * (width - thickness * 2),
                self.k_mm * (height - thickness * 2)
            ),
            self.pen_internal
        )

    def draw_cables(self, scene, cables, cable_diameter, channel_height, lineup_function):
        side_x, side_y, addon = lineup_function(cables, cable_diameter, channel_height)
        layers, tail = addon // side_y, addon % side_y
        shift = cable_diameter * self.k_mm

        # draw cables
        for i in range(side_x):
            for j in range(side_y):
                place = QtCore.QRectF(shift * i, shift * j, shift, shift)
                scene.addEllipse(place, pen=self.pen_cable)

        for i in range((layers + 1) if tail > 0 else layers):
            for j in range(side_y if tail == 0 or i < layers else tail):
                place = QtCore.QRectF(shift * (side_x + i), shift * j, shift, shift)
                scene.addEllipse(place, pen=self.pen_cable)

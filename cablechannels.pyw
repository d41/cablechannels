# -*- coding: utf-8 -*-

"""
Windows GUI entry point.
"""

import cablechannels

if "__main__" == __name__:
    cablechannels.run()

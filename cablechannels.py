# -*- coding: utf-8 -*-

import sys

from PyQt5 import QtWidgets

from modules.cablechannels import CableChannels

__author__ = "d41"


def run():
    app = QtWidgets.QApplication(sys.argv)
    """
    print(app.desktop().physicalDpiX())
    print(app.desktop().widthMM())

    from PyQt5 import QtGui
    dpi_logical = QtGui.QGuiApplication.primaryScreen().logicalDotsPerInch()
    dpi_physical = QtGui.QGuiApplication.primaryScreen().physicalDotsPerInch()
    print(dpi_physical / dpi_logical)
    print("logical: ", dpi_logical, "\nphysical: ", dpi_physical)
    print("device pixel ratio: ", QtGui.QGuiApplication.primaryScreen().devicePixelRatio())
    print("screen name: ", QtGui.QGuiApplication.primaryScreen().name())
    print("screen size: ", QtGui.QGuiApplication.primaryScreen().size())
    print("screen size: ", QtGui.QGuiApplication.primaryScreen().physicalSize())
    print("screen geo: ", QtGui.QGuiApplication.primaryScreen().availableGeometry())
    """

    win = CableChannels()
    win.show()
    sys.exit(app.exec_())


if "__main__" == __name__:
    run()
